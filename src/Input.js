import React from 'react';
import styled from 'styled-components';

const Input = ({...restProps}) => {
    const Input = styled.input`
        min-width: 20rem;
        padding: .5rem;
    `;
    return(
        <Input {...restProps} />
    )
}

export default Input;