import React, { Component } from 'react';
import Input from './Input';
import Recipe from './Recipe';
import styled from 'styled-components';

const API = 'http://www.recipepuppy.com/api/?&q=';

class App extends Component {
  state = {recipes : []}

  handleChange = ({target: {value}}) => {
    fetch(`${API}${value}`)
      .then(resp => {
        this.setState({ recipes: resp.results.slice(0, 20) });
      })
      .catch(err => {
        console.warn(err);
      })
  }

  render() {
    const Wrapper = styled.div`
      height: 100%;
      margin: 2rem;
      padding: 3rem;
      background-color: lightgrey;
    `;

    const { recipes } = this.state;
    return (
      <Wrapper>
        <h1>Recipe App</h1>
        <Input onChange={this.handleChange} placeholder="Search for recipes"/>
        <h2>Results...</h2>
        {recipes.map((recipe, id) => (
          <Recipe {...recipe} key={`recipe${id}`}/>
        ))}
      </Wrapper>
    );
  }
}

export default App;
