import Input from './Input';
import React from 'react';
import {shallow} from 'enzyme';

describe('Input', () => {
    it('renders', () => {
        const wrapper = shallow(<Input/>);
        expect(wrapper).toMatchSnapshot();
    })
})
