import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Recipe = ({title, href, ingredients, thumbnail}) => {
    const Title = styled.h3``;
    const Ingredients = styled.p``;
    const Thumb = styled.img`
        width: 50px;
        height: 50px;
    `;
    return(
        <React.Fragment>
            <Title><a href={href}>{title}</a></Title>
            <Ingredients>{ingredients}</Ingredients>
            {thumbnail.length > 0 && <Thumb src={thumbnail}/>}
        </React.Fragment>
    )
}

export default Recipe;

Recipe.propTypes = {
    title: PropTypes.string,
    href: PropTypes.string,
    ingredients: PropTypes.string,
    thumbnail: PropTypes.string,
}

Recipe.defaultProps = {
    title: '',
    href: '',
    ingredients: '',
    thumbnail: '',
}