import Input from './Input';
import React from 'react';
import {shallow} from 'enzyme';

describe('Input', () => {
    it('renders', () => {
        const wrapper = shallow(<Input/>);
        expect(wrapper).toMatchSnapshot();
    })
    it('renders with props', () => {
        const props = {
            title: 'My awesome recipe',
            ingredients: 'carrot, orange, milk',
            thumb: 'awesome_img.png',
            href: 'mypage.com/awesome',
        }
        const wrapper = shallow(<Input {...props}/>);
        expect(wrapper).toMatchSnapshot();
    })
})
